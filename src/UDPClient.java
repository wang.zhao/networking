import java.net.*;
import java.io.*;
import java.util.*;

class UDPClient implements Runnable {

	InetSocketAddress isA; // the remote address ( LOCAL HOST / PORT )
	DatagramSocket s; // the socket object ( SOCKET )
	DatagramPacket req, rep; // to prepare the request and reply messages ( PACKET ) // REQUEST TO SEND &
	private final int size = 2048; // the default size for the buffer array

	// 10.192.11.144
	/** The builder. */
	UDPClient() {
		isA = new InetSocketAddress("localhost", 8080);
		s = null;
		req = rep = null;
	}

	/** The main run method for threading. */
	public void run() {

		try {

			s = new DatagramSocket();

			req = new DatagramPacket(new byte[size], 0, size, isA.getAddress(), isA.getPort());
			s.send(req);
			System.out.println("Request sent");

			rep = new DatagramPacket(new byte[size], size);
			s.receive(rep);
			System.out.println("Reply received");

			s.close();

		}

		catch (IOException e) {
			System.out.println("IOException UDPClient");
		}

	}
}
