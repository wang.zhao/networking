import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

class UDPServerHello extends UDPServerBuilder implements Runnable {

	public void run() {
		try {
			s = new DatagramSocket(isA.getPort());
			s.setSoTimeout(10000);
			req = new DatagramPacket(new byte[size], size);

			s.receive(req);
			System.out.println("Request received from Server");
			rep = new DatagramPacket(new byte[size], 0, size, req.getSocketAddress());
			s.send(rep);
			System.out.println("Reply sent from Server");
			s.close();
		} catch (IOException e) {
			System.out.println("IOException UDPServer");
		}
	}
}